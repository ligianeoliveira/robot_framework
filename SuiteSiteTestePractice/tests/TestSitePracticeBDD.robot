*** Settings ***
Resource          ../resource/Resource.robot
##Test Setup        Abrir navegador
##Test Teardown     Fechar navegador

### SETUP ele roda keyword antes da suíte ou antes de um teste
### TEARDOWN ele roda keyword depois de uma suíte ou um teste

*** Test Case ***
Cenário 01: Pesquisar produtos existente
    ### [Setup]   Fechar navegador firefox -  nessa caso ele ignora o setup acima e executa esse
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "Blouse"
    Então o produto "Blouse" deve ser listado na página de resultado da busca
    ### [Teardown]   Fechar firefox -  nessa caso ele ignora o teardown acima e executa esse

Cenário 02: Pesquisar produtdo não existente
    Dado que estou na página home do site
    Quando eu pesquisar pelo produto "itemNãoExistente"
    Então a página deve exibir a mensagem "No results were found for your search "itemNãoExistente""

*** Keywords ***
Dado que estou na página home do site
    Acessar a página home do site

Quando eu pesquisar pelo produto "${PRODUTO}"
    Digitar o nome do produto "${PRODUTO}" no campo de pesquisa
