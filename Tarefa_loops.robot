*** Settings ***
Documentation   Tarefa sobre LOOPS!!

*** Variables ***
@{NUMBERS_LIST}            0  1  2  3  4  5  6  7  8  9  10  11  12

*** Test Case ***
Looking for a especific number
    [Documentation]  Faz um loop percorrendo a lista que você passar
    Looking for numbers 5 and 10 in a list

*** Keywords ***
Looking for numbers 5 and 10 in a list
    Log To Console    ${\n}
    FOR   ${NUMBER}   IN   @{NUMBERS_LIST}
      IF  ${NUMBER} == 5
        Log To Console    Eu sou o número ${NUMBER}!
      ELSE IF  ${NUMBER} == 10
        Log To Console    Eu sou o número ${NUMBER}!
      ELSE
        Log To Console    Eu não sou o número 5 e nem o 10!
      END
    END

## OUTRAS FORMAS
#Log To Console  ${\n}
  #FICA A DICA
  # Existem outros modos de fazer, veja abaixo:
#  FOR    ${numero}    IN   @{NUMEROS}
#      IF  ${numero} == 5 or ${numero} == 10
#          Log To Console    Eu sou o número ${numero}!
#      ELSE
#          Log To Console    Eu não sou o número 5 e nem o 10!
#      END
#  END

#  Log To Console  ${\n}
#  FOR    ${numero}    IN   @{NUMEROS}
#      IF  ${numero} in (5, 10)
#          Log To Console    Eu sou o número ${numero}!
#      ELSE
#          Log To Console    Eu não sou o número 5 e nem o 10!
#      END
#  END
