*** Settings ***
Documentation   Documentação da API: https://fakerestapi.azurewebsites.net/index.html
Library         RequestsLibrary
Library         Collections

*** Variable ***
${URL_API}     https://fakerestapi.azurewebsites.net/api/
&{BOOK_15}     id=15
...            title=Book 15
...            pageCount=1500

&{NEW_BOOK}    id=4000
...            title=Teste_ligi
...            description=teste
...            pageCount=1000
...            excerpt=testes

*** Keywords ***
# SETUPS e TEARDOWNS
Conectar a API
    Create Session    fakeAPI    ${URL_API}

#AÇÕES
Requisitar todos os livros
    ${RESPONSE}        GET On Session   fakeAPI   v1/Books   expected_status=any
    Log                ${RESPONSE.text}
    #essa keyword serve pra tornar a váriavel visível para todos os testes desse resource
    Set Test Variable  ${RESPONSE}

Requisitar o livro "${ID_LIVRO}"
    ${RESPONSE}        GET On Session   fakeAPI   v1/Books/${ID_LIVRO}   expected_status=any
    Log                ${RESPONSE.text}
    Set Test Variable  ${RESPONSE}

Cadastrar um novo livro
    ${HEADERS}         Create Dictionary  content-type=application/json
    ${RESPONSE}        POST On Session    fakeAPI   v1/Books   expected_status=200
    #esses ... indicam que é uma quebra de linha
    ...                                   data={"id": 4000, "title": "Teste_ligi", "description": "teste", "pageCount": 1000, "excerpt": "testes", "publishDate": "2021-11-28T18:38:32.977Z"}
    ...                                   headers=${HEADERS}
    Log                ${RESPONSE.text}
    Set Test Variable  ${RESPONSE}

Alterar o livro "${ID_LIVRO}"
    ${HEADERS}         Create Dictionary  content-type=application/json
    ${RESPONSE}        PUT On Session     fakeAPI   v1/Books/${ID_LIVRO}   expected_status=200
    ...                                   data={"id": ${ID_LIVRO}, "title": "titulo_alterado", "description": "alterado", "pageCount": 100, "excerpt": "alterado", "publishDate": "2021-11-28T19:16:08.977Z"}
    ...                                   headers=${HEADERS}
    Log                ${RESPONSE.text}
    Set Test Variable  ${RESPONSE}


#CONFERÊNCIAS
Conferir o status code
    [Arguments]                      ${STATUSCODE_DESEJADO}
    Should Be Equal As Strings       ${RESPONSE.status_code}   ${STATUSCODE_DESEJADO}

Conferir o reason
    [Arguments]                      ${REASON_DESEJADO}
    Should Be Equal As Strings       ${RESPONSE.reason}        ${REASON_DESEJADO}

Conferir se retorna uma lista com "${QTDE_LIVROS}" livros
    Length Should Be                 ${RESPONSE.json()}        ${QTDE_LIVROS}

Conferir se retorna todos os dados corretos do livro 15
    Dictionary Should Contain Item   ${RESPONSE.json()}        id            ${BOOK_15.id}
    Dictionary Should Contain Item   ${RESPONSE.json()}        title         ${BOOK_15.title}
    Dictionary Should Contain Item   ${RESPONSE.json()}        pageCount     ${BOOK_15.pageCount}
    #os campos abaixo foram validados como não podem ser vazios pois eles são aleatórios a cada chamada
    Should Not Be Empty              ${RESPONSE.json()["description"]}
    Should Not Be Empty              ${RESPONSE.json()["excerpt"]}
    Should Not Be Empty              ${RESPONSE.json()["publishDate"]}

Conferir se retorna todos os dados cadastrados para o novo livro
    Dictionary Should Contain Item   ${RESPONSE.json()}        id            ${NEW_BOOK.id}
    Dictionary Should Contain Item   ${RESPONSE.json()}        title         ${NEW_BOOK.title}
    Dictionary Should Contain Item   ${RESPONSE.json()}        description   ${NEW_BOOK.description}
    Dictionary Should Contain Item   ${RESPONSE.json()}        pageCount     ${NEW_BOOK.pageCount}
    Dictionary Should Contain Item   ${RESPONSE.json()}        excerpt       ${NEW_BOOK.excerpt}
